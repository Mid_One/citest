<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends CI_Controller
{
	protected $data = array();
	protected $page_count = 25;
	protected $user_table = 'user';
	protected $club_table = 'club';


	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Taipei");

//		if ($this->session->isLogin && $this->encryption->decrypt($this->session->isLogin) == md5("uLogIn")) {
//			$this->data['isLogin'] = TRUE;
//		} else {
//			$this->data['isLogin'] = FALSE;
//		}

//		$settings = $this->db->get("settings")->result_array();
//		$setting = [];
//		foreach ($settings as $key => $s) {
//			$setting[$s["title"]] = $s["content"];
//		}
//		$this->data["setting"] = $setting;
	}

	public function flow_record($enter)
	{
		if ($this->db->where("create_date like '" . date("Y-m-d") . "%' AND ip = '" . $this->get_client_ip() . "' AND enter='{$enter}'")->count_all_results("flow") <= 0) {
			$this->db->insert("flow", array("ip" => $this->get_client_ip(), "enter" => $enter));
		}
	}

	protected function response($code, $msg, $data = FALSE)
	{
		if ($data === FALSE) {
			echo json_encode(array("status" => $code, "msg" => $msg),JSON_UNESCAPED_UNICODE);
		} else {
			$response['status'] = $code;
			$response['msg'] = $msg;
			$response['data'] = $data;
			echo json_encode($response,JSON_UNESCAPED_UNICODE);
		}
		exit();
	}

	protected function js_output_and_back($msg)
	{
		echo "<script> alert('" . $msg . "'); history.back(); </script>";
		exit();
	}

	protected function js_output_and_redirect($msg, $url)
	{
		echo "<script> alert('" . $msg . "'); location.href='" . $url . "'; </script>";
		exit();
	}

	protected function loading_item()
	{
		$items = "";
		for ($i = 0; $i < 3; $i++) {
			$items .= $this->load->view("loading_item", array(), true);
		}
		return $items;
	}

	public function is_login()
	{
		if (!($this->session->isLogin && $this->encryption->decrypt($this->session->isLogin) == md5("uLogIn"))) return FALSE;
		return TRUE;
	}

	protected function is_mgr_lock()
	{
		if ($this->session->has_userdata('lock') && $this->session->has_userdata('isMgrlogin') && $this->encryption->decrypt($this->session->isMgrlogin) == md5("MGRLOGIN")) {
			header("Location: " . base_url() . "mgr/lock");
		}
	}

	protected function is_mgr_login()
	{
		$this->is_mgr_lock();

		if ($this->session->has_userdata('isMgrlogin') && $this->encryption->decrypt($this->session->isMgrlogin) == md5("MGRLOGIN")) {
			// }else if($this->input->cookie('al', TRUE) && $this->input->cookie('al', TRUE)!=""){
			// 	$decrypt_token = $this->encryption->decrypt($this->input->cookie('al', TRUE));
			// 	$this->auto_login($decrypt_token);
		} else {
			//強制登出
			// $this->session->sess_destroy();

			header("Location: " . base_url() . "mgr/login");
		}
	}

	protected function set_upload_options($dir = 'uploads/')
	{
		if (!file_exists($dir)) {
			$oldmask = umask(0);
			mkdir($dir, 0755);
			umask($oldmask);
		}

		$config = array();
		$config['upload_path'] = $dir;
		$config['allowed_types'] = '*';//'gif|jpg|png|jpeg';
		$config['max_size'] = '0';
		$config['overwrite'] = FALSE;
		$config['encrypt_name'] = TRUE;

		return $config;
	}

	protected function get_client_ip()
	{
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if (isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

}
